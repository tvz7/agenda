import java.util.Scanner;
public class Principal {
  public static void main(String[] args){
    Scanner s = new Scanner(System.in);
    Agenda agenda = new Agenda();
    System.out.println("Qual o nome da pessoa ?");
    String nome = s.nextLine();

    System.out.println("Qual a idade da pessoa ?");
    int idade = s.nextInt();

    System.out.println("Qual a altura da pessoa ?");
    float altura = s.nextFloat();

    agenda.armazenarPessoa(nome, idade, altura);
    agenda.imprimeAgenda();

    if(agenda.buscarPessoa("Wagner") == -1){
      System.out.println("Usuario não existe");
    } else {
      System.out.println("Index: " + agenda.buscarPessoa("Wagner"));
    }
  }
}
